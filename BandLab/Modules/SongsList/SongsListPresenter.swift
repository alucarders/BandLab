//
//  SongsListPresenter.swift
//  BandLab
//
//  Created by Maksim Kupetskii on 07/04/2018.
//  Copyright © 2018 Maksim Kupetskii. All rights reserved.
//

import Foundation

protocol SongsListViewPresenterProtocol {
    var songs: [SongEntity] { get }
    func viewLoaded()
    func downloadSong(withId songId: Int)
    func playSong(withId songId: Int)
    func pausePlaying()
    func redownloadAll()
}

protocol SongsListInteractorPresenterProtocol: class {
    func didLoadSongs()
    func didDownloadSong(_ song: SongEntity)
    func didPaused(song: SongEntity)
    func didUpdateProgress(for songId: Int, progress: Float)
}

final class SongsListPresenter {

    let router: SongsListPresenterRouterProtocol
    let interactor: SongsListPresenterInteractorProtocol

    weak var view: SongsListPresenterViewProtocol?

    init(router: SongsListPresenterRouterProtocol, interactor: SongsListPresenterInteractorProtocol) {
        self.router = router
        self.interactor = interactor
    }
}

// MARK: - SongsListViewPresenterProtocol
extension SongsListPresenter: SongsListViewPresenterProtocol {
    var songs: [SongEntity] {
        return interactor.songs
    }

    func viewLoaded() {
        interactor.getSongsList()
    }

    func downloadSong(withId songId: Int) {
        interactor.downloadSong(withId: songId)
    }

    func playSong(withId songId: Int) {
        interactor.playSong(withId: songId)
    }

    func pausePlaying() {
        interactor.pausePlaying()
    }

    func redownloadAll() {
        interactor.redownloadEverything()
    }
}

// MARK: - SongsListInteractorPresenterProtocol
extension SongsListPresenter: SongsListInteractorPresenterProtocol {
    func didLoadSongs() {
        view?.updateTableView()
    }

    func didDownloadSong(_ song: SongEntity) {
        view?.updateSongCell(withSong: song)
    }

    func didPaused(song: SongEntity) {
        view?.updateSongCell(withSong: song)
    }

    func didUpdateProgress(for songId: Int, progress: Float) {
        view?.updateCellProgress(cellId: songId, progress: progress)
    }
}
