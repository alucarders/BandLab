//
//  RootPresenter.swift
//  BandLab
//
//  Created by Maksim Kupetskii on 07/04/2018.
//  Copyright © 2018 Maksim Kupetskii. All rights reserved.
//

import Foundation

protocol RootViewPresenterProtocol {
    func viewLoaded()
}

protocol RootInteractorPresenterProtocol: class {}

final class RootPresenter {

    let router: RootPresenterRouterProtocol
    let interactor: RootPresenterInteractorProtocol

    weak var view: RootPresenterViewProtocol?

    init(router: RootPresenterRouterProtocol, interactor: RootPresenterInteractorProtocol) {
        self.router = router
        self.interactor = interactor
    }
}

extension RootPresenter: RootViewPresenterProtocol {
    func viewLoaded() {
        router.showSongsList()
    }
}

extension RootPresenter: RootInteractorPresenterProtocol {}
