//
//  SongEndpoint.swift
//  BandLab
//
//  Created by Maksim Kupetskii on 07/04/2018.
//  Copyright © 2018 Maksim Kupetskii. All rights reserved.
//

import Foundation

public enum SongApi {
    case fullList
    case song(atURL: String)
}

extension SongApi: EndPointType {

    private var fullURL: String {
        return "https://gist.githubusercontent.com/" +
                "Lenhador/a0cf9ef19cd816332435316a2369bc00/raw/" +
                "9302546abb4552605d71d8da51d1f97bc039d431/"
    }

    var baseURL: URL {
        let burl: String
        switch self {
        case .fullList: burl = fullURL
        case .song(let url): burl = url
        }
        guard let url = URL(string: burl) else { fatalError("baseURL fail") }
        return url
    }

    var path: String {
        switch self {
        case .fullList:
            return "Songs.json"
        case .song:
            return ""
        }
    }

    var httpMethod: HTTPMethod {
        return .get
    }

    var task: HTTPTask {
        switch self {
        case .fullList:
            return .request
        case .song:
            return .download
        }
    }

    var headers: HTTPHeaders? {
        return nil
    }
}
