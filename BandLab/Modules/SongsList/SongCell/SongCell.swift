//
//  SongCell.swift
//  BandLab
//
//  Created by Maksim Kupetskii on 07/04/2018.
//  Copyright © 2018 Maksim Kupetskii. All rights reserved.
//

import UIKit

protocol SongCellDelegate: class {
    func downloadButtonPressed(cellTag: Int)
    func playButtonPressed(cellTag: Int)
    func pauseButtonPressed()
}

class SongCell: UITableViewCell {

    static var reuseId: String {
        return String(describing: SongCell.self)
    }
    static let height: CGFloat = 138

    weak var songCellDelegate: SongCellDelegate?

    @IBOutlet weak var colorBackgroundView: UIView!
    @IBOutlet weak var songNameLabel: UILabel!
    @IBOutlet weak var dlButton: UIButton!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var progressView: CircleProgressView!

    override func awakeFromNib() {
        super.awakeFromNib()
        colorBackgroundView.backgroundColor = Constants.Colors.cellBackground
        colorBackgroundView.layer.cornerRadius = 8.0
        songNameLabel.font = Constants.Fonts.cellFont
        songNameLabel.textColor = Constants.Colors.cellFont
    }

    func configure(with song: SongEntity) {
        songNameLabel.text = song.name ?? ""
        dlButton.isHidden = song.isDownloaded
        playButton.setImage(song.isPlaying ? #imageLiteral(resourceName: "pause") : #imageLiteral(resourceName: "play"), for: .normal)
        playButton.isHidden = !dlButton.isHidden
        tag = Int(song.id)
    }

    func updateProgress(ratio: Float) {
        print(ratio)
    }

    // MARK: - Actions
    @IBAction func downloadButtonPressed(sender: UIButton) {
        dlButton.setImage(#imageLiteral(resourceName: "loadingIndicator"), for: .normal)
//        dlButton.isHidden = true
//        progressView.isHidden = false
        dlButton.isUserInteractionEnabled = false
        songCellDelegate?.downloadButtonPressed(cellTag: tag)
    }

    @IBAction func playButtonPressed(sender: UIButton) {
        if sender.imageView?.image == #imageLiteral(resourceName: "play") {
            sender.setImage(#imageLiteral(resourceName: "pause"), for: .normal)
            songCellDelegate?.playButtonPressed(cellTag: tag)
        } else {
            sender.setImage(#imageLiteral(resourceName: "play"), for: .normal)
            songCellDelegate?.pauseButtonPressed()
        }
    }
}
