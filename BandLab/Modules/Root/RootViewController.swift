//
//  RootViewController.swift
//  BandLab
//
//  Created by Maksim Kupetskii on 07/04/2018.
//  Copyright © 2018 Maksim Kupetskii. All rights reserved.
//

import UIKit

protocol RootPresenterViewProtocol: class {}

class RootViewController: UIViewController {

    let presenter: RootViewPresenterProtocol

    init(presenter: RootViewPresenterProtocol) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

//        networkManager.getSong(at: "https://a.clyp.it/pezpcfah.mp3") { error in
//            print("suss")
//        }

        presenter.viewLoaded()
    }
}

extension RootViewController: RootPresenterViewProtocol {}
