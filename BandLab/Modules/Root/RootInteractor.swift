//
//  RootInteractor.swift
//  BandLab
//
//  Created by Maksim Kupetskii on 07/04/2018.
//  Copyright © 2018 Maksim Kupetskii. All rights reserved.
//

import Foundation

protocol RootPresenterInteractorProtocol {
}

final class RootInteractor {
    weak var presenter: RootInteractorPresenterProtocol?
}

// MARK: - Root Presenter to Interactor Protocol
extension RootInteractor: RootPresenterInteractorProtocol {
}
