//
//  SongsListModule.swift
//  BandLab
//
//  Created by Maksim Kupetskii on 07/04/2018.
//  Copyright © 2018 Maksim Kupetskii. All rights reserved.
//

import UIKit

class SongsListModule {

    let storyboard: UIStoryboard = UIStoryboard(name: Constants.Storyboards.main,
                                                bundle: Bundle.main)

    private(set) lazy var interactor: SongsListInteractor = {
        SongsListInteractor()
    }()

    private(set) lazy var router: SongsListRouter = {
        SongsListRouter()
    }()

    private(set) lazy var presenter: SongsListPresenter = {
        SongsListPresenter(router: self.router, interactor: self.interactor)
    }()

    private(set) lazy var view: SongsListViewController = {
        let viewC = self.storyboard.viewController(SongsListViewController.self)
        viewC.presenter = self.presenter
        return viewC
    }()

    var viewController: UIViewController { return view }

    init(networkManager: NetworkManager = NetworkManager(),
         fileManager: PersistenceManager = SongFileManager(),
         audioPlayer: AudioPlayer = SongPlayer()) {
        presenter.view = view
        router.viewController = view
        interactor.presenter = presenter

        interactor.networkManager = networkManager
        networkManager.setProgressDelegate(interactor)
        interactor.songFileManager = fileManager
        interactor.audioPlayer = audioPlayer
    }
}
