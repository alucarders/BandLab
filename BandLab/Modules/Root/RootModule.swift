//
//  RootModule.swift
//  BandLab
//
//  Created by Maksim Kupetskii on 07/04/2018.
//  Copyright © 2018 Maksim Kupetskii. All rights reserved.
//

import UIKit

final class RootModule {

    private(set) lazy var interactor: RootInteractor = {
        RootInteractor()
    }()

    private(set) lazy var router: RootRouter = {
        RootRouter()
    }()

    private(set) lazy var presenter: RootPresenter = {
        RootPresenter(router: self.router, interactor: self.interactor)
    }()

    private(set) lazy var view: RootViewController = {
        RootViewController(presenter: self.presenter)
    }()

    var viewController: UIViewController {
        return view
    }

    init() {
        presenter.view = view
        router.viewController = view
        interactor.presenter = presenter
    }
}
