//
//  SongsListInteractor.swift
//  BandLab
//
//  Created by Maksim Kupetskii on 07/04/2018.
//  Copyright © 2018 Maksim Kupetskii. All rights reserved.
//

import Foundation
import CoreData

protocol SongsListPresenterInteractorProtocol {
    var songs: [SongEntity] { get }
    func getSongsList()
    func downloadSong(withId songId: Int)
    func playSong(withId songId: Int)
    func pausePlaying()
    func redownloadEverything()
}

final class SongsListInteractor {
    weak var presenter: SongsListInteractorPresenterProtocol?

    var networkManager: NetworkManager!
    var songFileManager: PersistenceManager!
    var audioPlayer: AudioPlayer!

    internal var songs: [SongEntity] = []
    private var songsUrls: [String] = []

    private func fetchSongsCD() {
        let fetchRequest: NSFetchRequest<SongEntity> = SongEntity.fetchRequest()
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "id", ascending: true)]

        do {
            songs = try CoreDataStack.shared.moc.fetch(fetchRequest)
            songs.forEach { song in
                guard let dlUrl = song.dlUrl else { fatalError() }
                self.songsUrls.append(dlUrl)
            }
        } catch {
            print(error)
        }
    }

    private func downloadSongs() {
        networkManager.getSongsList { [unowned self] songs, error in
            if let error = error {
                print(error)
            } else if let songs = songs {
                for song in songs {
                    guard let fileName = song.fileName,
                        let dlUrl = song.dlUrl else { continue }

                    let songEntity = SongEntity(context: CoreDataStack.shared.moc)
                    songEntity.id = Int64(song.id)
                    songEntity.name = song.name
                    songEntity.audioUrl = song.audioURL
                    songEntity.dlUrl = dlUrl
                    songEntity.fileName = fileName
                    if self.songFileManager.flleExists(fileName) {
                        songEntity.isDownloaded = true
                    }
                }
                CoreDataStack.shared.saveContext()
                self.fetchSongsCD()
                self.presenter?.didLoadSongs()
            }
        }
    }
}

extension SongsListInteractor: SongsListPresenterInteractorProtocol {
    func getSongsList() {
        fetchSongsCD()
        if songs.isEmpty {
            downloadSongs()
        } else {
            presenter?.didLoadSongs()
        }
    }

    func downloadSong(withId songId: Int) {
        for (index, song) in songs.enumerated() where song.id == songId {
            guard let url = song.dlUrl else { continue }
            networkManager.getSong(at: url) { [unowned self] tmpUrl, error in
                if let error = error {
                    print(error)
                } else if let tmpUrl = tmpUrl {
                    guard let fileName = song.fileName else { return }
                    self.songFileManager.saveFile(from: tmpUrl, named: fileName)

                    self.songs[index].isDownloaded = true
                    CoreDataStack.shared.saveContext()

                    self.presenter?.didDownloadSong(song)
                }
            }
        }
    }

    func playSong(withId songId: Int) {
        pausePlaying()
        for (index, song) in songs.enumerated() where song.id == songId {
            guard let fileName = song.fileName else { return }
            let url = songFileManager.getFile(named: fileName)

            self.songs[index].isPlaying = true
            audioPlayer.playFile(at: url)
        }
    }

    func pausePlaying() {
        for (index, song) in songs.enumerated() where song.isPlaying {
            songs[index].isPlaying = false
            audioPlayer.pause()
            presenter?.didPaused(song: song)
        }
    }

    func redownloadEverything() {
        songFileManager.deleteAll()
        for song in songs {
            CoreDataStack.shared.moc.delete(song)
        }
        presenter?.didLoadSongs()
        downloadSongs()
    }
}

extension SongsListInteractor: DownloadProgressDelegate {
    func progressUpdated(for url: URL, progress: Float) {
        guard let index = songsUrls.index(of: url.absoluteString) else { return }
        presenter?.didUpdateProgress(for: index, progress: progress)
    }
}
