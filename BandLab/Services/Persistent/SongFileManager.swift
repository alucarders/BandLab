//
//  SongFileManager.swift
//  BandLab
//
//  Created by Maksim Kupetskii on 08/04/2018.
//  Copyright © 2018 Maksim Kupetskii. All rights reserved.
//

import Foundation

protocol PersistenceManager {
    func flleExists(_ fileName: String) -> Bool
    func saveFile(from oldUrl: URL, named fileName: String)
    func getFile(named fileName: String) -> URL
    func deleteAll()
}

class SongFileManager {

    private let fileManager = FileManager.default
    private var documentsUrl: URL {
        if let url = fileManager.urls(for: .documentDirectory,
                                      in: .userDomainMask).first {
            return url
        } else {
            fatalError("Could not retrieve documents directory")
        }
    }
}

extension SongFileManager: PersistenceManager {

    func flleExists(_ fileName: String) -> Bool {
        let filePath = getFile(named: fileName).path
        return fileManager.fileExists(atPath: filePath)
    }

    func saveFile(from oldUrl: URL, named fileName: String) {
        let newUrl = documentsUrl.appendingPathComponent(fileName)
        do {
            try fileManager.copyItem(at: oldUrl, to: newUrl)
        } catch {
            print("Error: \(error.localizedDescription)")
        }
    }

    func getFile(named fileName: String) -> URL {
        let fileUrl = documentsUrl.appendingPathComponent(fileName)
        return fileUrl
    }

    func deleteAll() {
        do {
            let files = try fileManager.contentsOfDirectory(at: documentsUrl,
                                                            includingPropertiesForKeys: nil,
                                                            options: [])
            for file in files {
                try fileManager.removeItem(at: file)
            }
        } catch {
            print(error)
        }
    }
}
