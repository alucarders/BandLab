//
//  SongListModel.swift
//  BandLab
//
//  Created by Maksim Kupetskii on 07/04/2018.
//  Copyright © 2018 Maksim Kupetskii. All rights reserved.
//

import Foundation

struct SongListApiResponse {
    let songs: [Song]
}

extension SongListApiResponse: Decodable {
    private enum ResponseKeys: String, CodingKey {
        case songs = "data"
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: ResponseKeys.self)
        songs = try container.decode([Song].self, forKey: .songs)
    }
}

// MARK: - Single Song
struct Song {
    // swiftlint:disable identifier_name
    let id: Int
    // swiftlint:enable identifier_name
    let name: String
    let audioURL: String
    var isDownloaded = false
    var isPlaying = false

    var dlUrl: String? {
        var url = audioURL
        guard let fname = fileName else { return url }

        if audioURL =~ Constants.Networking.googleDrive {
            url = Constants.Networking.gDriveLink +
                fname + Constants.Networking.gApiKey
        }
        return url
    }

    var fileName: String? {
        guard let url = URL(string: audioURL) else { return nil }

        let gDriveLink = audioURL =~ Constants.Networking.googleDrive
        if gDriveLink {
            if audioURL =~ Constants.Networking.googleDriveOpen {
                guard let query = url.query else { return nil }
                return query.replacingOccurrences(of: Constants.Networking.googleDriveId,
                                                  with: "")
            } else {
                let pathComps = url.pathComponents
                guard let fileIndex = pathComps.index(of: Constants.Networking.googlePathFile),
                      pathComps.count > (fileIndex + 2) else {
                    return nil
                }
                return pathComps[fileIndex + 2]
            }
        }

        return url.lastPathComponent
    }
}

extension Song: Decodable {
    private enum SongKeys: String, CodingKey {
        // swiftlint:disable identifier_name
        case id
        // swiftlint:enable identifier_name
        case name
        case audioURL
    }

    init(from decoder: Decoder) throws {
        let songContainer = try decoder.container(keyedBy: SongKeys.self)

        guard let intId = try Int(songContainer.decode(String.self, forKey: .id)) else {
            throw DecodingError.dataCorrupted(.init(codingPath: [SongKeys.id],
                                                    debugDescription: "Couldn't cast from String to Int"))
        }
        id = intId
        name = try songContainer.decode(String.self, forKey: .name)
        audioURL = try songContainer.decode(String.self, forKey: .audioURL)
    }
}
