//
//  SongsListRouter.swift
//  BandLab
//
//  Created by Maksim Kupetskii on 07/04/2018.
//  Copyright © 2018 Maksim Kupetskii. All rights reserved.
//

import UIKit

protocol SongsListPresenterRouterProtocol {}

final class SongsListRouter {
    weak var viewController: UIViewController?
}

extension SongsListRouter: SongsListPresenterRouterProtocol {}
