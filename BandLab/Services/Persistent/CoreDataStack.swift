//
//  CoreDataStack.swift
//  BandLab
//
//  Created by Maksim Kupetskii on 08/04/2018.
//  Copyright © 2018 Maksim Kupetskii. All rights reserved.
//

import Foundation
import CoreData

class CoreDataStack {

    private init() {}
    static let shared = CoreDataStack()

    // MARK: - Core Data stack
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "BandLab")
        container.loadPersistentStores(completionHandler: { _, error
            in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    lazy var moc: NSManagedObjectContext = {
        return self.persistentContainer.viewContext
    }()

    // MARK: - Core Data Saving support
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}
