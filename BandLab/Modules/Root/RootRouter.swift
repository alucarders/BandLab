//
//  RootRouter.swift
//  BandLab
//
//  Created by Maksim Kupetskii on 07/04/2018.
//  Copyright © 2018 Maksim Kupetskii. All rights reserved.
//

import UIKit

protocol RootPresenterRouterProtocol {
    func showSongsList()
}

final class RootRouter {
    weak var viewController: UIViewController?
}

extension RootRouter: RootPresenterRouterProtocol {
    func showSongsList() {
        let songsViewController = SongsListModule().view
        viewController?.addChildViewController(songsViewController)
        viewController?.view.addSubview(songsViewController.view)
    }
}
