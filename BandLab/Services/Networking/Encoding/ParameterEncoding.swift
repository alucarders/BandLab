//
//  ParameterEncoding.swift
//  BandLab
//
//  Created by Maksim Kupetskii on 07/04/2018.
//  Copyright © 2018 Maksim Kupetskii. All rights reserved.
//

import Foundation

public typealias Parameters = [String: Any]

public protocol ParameterEncoder {
    static func encode(urlRequest: inout URLRequest,
                       with parameters: Parameters) throws
}

public enum NetworkError: String, Error {
    case parametersNil  = "Parameters are nil"
    case encodingFailed = "Parameters encoding failed"
    case missingURL     = "URL is nil"
}
