//
//  RegexOperator.swift
//  BandLab
//
//  Created by Maksim Kupetskii on 08/04/2018.
//  Copyright © 2018 Maksim Kupetskii. All rights reserved.
//

import Foundation

infix operator =~: ComparisonPrecedence
func =~ (string: String, regex: String) -> Bool {
    return string.range(of: regex, options: .regularExpression) != nil
}
