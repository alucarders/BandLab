//
//  NetworkManager.swift
//  BandLab
//
//  Created by Maksim Kupetskii on 07/04/2018.
//  Copyright © 2018 Maksim Kupetskii. All rights reserved.
//

import Foundation

protocol DownloadProgressDelegate: class {
    func progressUpdated(for url: URL, progress: Float)
}

struct NetworkManager {
    private let router = Router<SongApi>()

    func setProgressDelegate(_ delegate: DownloadProgressDelegate?) {
        router.dlProgressDelegate = delegate
    }

    func getSongsList(completion: @escaping (_ songs: [Song]?, _ error: String?) -> Void) {
        router.request(.fullList) { data, response, error in
            if error != nil {
                completion(nil, "Check connection")
            }

            if let response = response as? HTTPURLResponse {
                let result = self.handleNetworkResponse(response)
                switch result {
                case .success:
                    guard let responseData = data else {
                        return completion(nil, NetworkResponse.noData.rawValue)
                    }
                    do {
                        let apiResponse = try JSONDecoder().decode(SongListApiResponse.self,
                                                                   from: responseData)
                        completion(apiResponse.songs, nil)
                    } catch {
                        completion(nil, NetworkResponse.unableToDecode.rawValue)
                    }

                case .failure(let networkFailureError):
                    completion(nil, networkFailureError)
                }
            }
        }
    }

    func getSong(at urlString: String,
                 completion: @escaping (_ tmpUrl: URL?, _ error: String?) -> Void) {
        router.download(.song(atURL: urlString)) { url, response, error in
            if error != nil {
                completion(nil, "Check connection")
            }

            if let response = response as? HTTPURLResponse {
                let result = self.handleNetworkResponse(response)
                switch result {
                case .success:
                    completion(url, nil)
                case .failure(let error):
                    completion(nil, error)
                }
            }
        }
    }

    // MARK: - Private
    private func handleNetworkResponse(_ response: HTTPURLResponse) -> Result<String> {
        switch response.statusCode {
        case 200...299: return .success
        case 401...500: return .failure(NetworkResponse.authenticationError.rawValue)
        case 501...599: return .failure(NetworkResponse.badRequest.rawValue)
        case 600: return .failure(NetworkResponse.outdated.rawValue)
        default: return .failure(NetworkResponse.failed.rawValue)
        }
    }
}

enum NetworkResponse: String {
    case success
    case authenticationError = "Not authenticated"
    case badRequest          = "Bad request"
    case outdated            = "The url is outdated"
    case failed              = "Network request failed"
    case noData              = "Response has no data"
    case unableToDecode      = "Response could not be decoded"
}

enum Result<String> {
    case success
    case failure(String)
}
