//
//  Router.swift
//  BandLab
//
//  Created by Maksim Kupetskii on 07/04/2018.
//  Copyright © 2018 Maksim Kupetskii. All rights reserved.
//

import Foundation

class Router<EndPoint: EndPointType>: NSObject, URLSessionDownloadDelegate {
    weak var dlProgressDelegate: DownloadProgressDelegate?

    private var task: URLSessionTask?
    private var downloads: [URL: NetworkDownloadCompletion] = [:]

    func urlSession(_ session: URLSession,
                    downloadTask: URLSessionDownloadTask,
                    didFinishDownloadingTo location: URL) {
        guard let url = downloadTask.originalRequest?.url,
              let completion = downloads[url] else { return }
        completion(location, downloadTask.response, nil)
        downloads[url] = nil
    }

    func urlSession(_ session: URLSession,
                    downloadTask: URLSessionDownloadTask,
                    didWriteData bytesWritten: Int64,
                    totalBytesWritten: Int64,
                    totalBytesExpectedToWrite: Int64) {
        guard let url = downloadTask.originalRequest?.url else { return }
        let progress = Float(totalBytesWritten) / Float(totalBytesExpectedToWrite)
        dlProgressDelegate?.progressUpdated(for: url, progress: progress)
    }
}

extension Router: NetworkRouter {

    func request(_ route: EndPoint, completion: @escaping NetworkRouterCompletion) {
        let session = URLSession.shared
        let request = buildRequest(from: route)
        task = session.dataTask(with: request) { data, response, error in
            completion(data, response, error)
        }
        task?.resume()
    }

    func download(_ route: EndPoint, completion: @escaping NetworkDownloadCompletion) {
        let session = URLSession(configuration: URLSessionConfiguration.default,
                                 delegate: self,
                                 delegateQueue: nil)
        let request = buildRequest(from: route)
        if let url = request.url {
            downloads[url] = completion
        }
        task = session.downloadTask(with: request)
        task?.resume()
    }

    // MARK: - Private
    private func buildRequest(from route: EndPoint) -> URLRequest {
        let url = route.path == "" ? route.baseURL :
                                     route.baseURL.appendingPathComponent(route.path)
        var request = URLRequest(url: url,
                                 cachePolicy: .reloadIgnoringLocalAndRemoteCacheData,
                                 timeoutInterval: 30.0)
        request.httpMethod = route.httpMethod.rawValue

        switch route.task {
        case .request:
            request.setValue(Constants.Networking.jsonContentType,
                             forHTTPHeaderField: Constants.Networking.contentType)

        case .download:
            request.setValue(Constants.Networking.audioContentType,
                             forHTTPHeaderField: Constants.Networking.contentType)
        }
        return request
    }
}
