//
//  SongsListViewController.swift
//  BandLab
//
//  Created by Maksim Kupetskii on 07/04/2018.
//  Copyright © 2018 Maksim Kupetskii. All rights reserved.
//

import UIKit

protocol SongsListPresenterViewProtocol: class {
    func updateTableView()
    func updateSongCell(withSong song: SongEntity)
    func updateCellProgress(cellId: Int, progress: Float)
}

class SongsListViewController: UIViewController {

    var presenter: SongsListViewPresenterProtocol?

    private lazy var refreshControl: UIRefreshControl = {
        let refControl = UIRefreshControl()
        let attrs: [NSAttributedStringKey: Any] = [.font: Constants.Fonts.refreshFont as Any,
                                                   .foregroundColor: Constants.Colors.refreshFont]
        refControl.attributedTitle = NSAttributedString(string: "Pull to redownload everything",
                                                        attributes: attrs)
        refControl.addTarget(self, action: #selector(redownloadAll), for: .valueChanged)
        return refControl
    }()

    private lazy var songsListTableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .plain)
        tableView.register(UINib(nibName: "SongCell", bundle: nil),
                           forCellReuseIdentifier: SongCell.reuseId)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.backgroundColor = UIColor.clear
        tableView.separatorStyle = .none
        tableView.allowsSelection = false
        tableView.addSubview(refreshControl)
        return tableView
    }()

    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        view.addSubview(songsListTableView)
        presenter?.viewLoaded()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        songsListTableView.frame.origin.x = view.safeAreaInsets.left
        songsListTableView.frame.origin.y = view.safeAreaInsets.top
        songsListTableView.frame.size.width = view.bounds.width - view.safeAreaInsets.left - view.safeAreaInsets.right
        songsListTableView.frame.size.height = view.bounds.height - view.safeAreaInsets.top - view.safeAreaInsets.bottom
    }

    @objc func redownloadAll() {
        presenter?.redownloadAll()
    }
}

// MARK: - SongsListPresenterViewProtocol
extension SongsListViewController: SongsListPresenterViewProtocol {
    func updateTableView() {
        DispatchQueue.main.async {
            self.refreshControl.endRefreshing()
            self.songsListTableView.reloadData()
        }
    }

    func updateSongCell(withSong song: SongEntity) {
        let indexPath = IndexPath.init(row: Int(song.id), section: 0)
        DispatchQueue.main.async {
            self.songsListTableView.reloadRows(at: [indexPath], with: .none)
        }
    }

    func updateCellProgress(cellId: Int, progress: Float) {
        let indexPath = IndexPath.init(row: cellId, section: 0)
        DispatchQueue.main.async {
            if let songCell = self.songsListTableView.cellForRow(at: indexPath)
                as? SongCell {
                songCell.updateProgress(ratio: progress)
            }
        }
    }
}

// MARK: - SongCellDelegate
extension SongsListViewController: SongCellDelegate {
    func downloadButtonPressed(cellTag: Int) {
        presenter?.downloadSong(withId: cellTag)
    }

    func playButtonPressed(cellTag: Int) {
        presenter?.playSong(withId: cellTag)
    }

    func pauseButtonPressed() {
        presenter?.pausePlaying()
    }
}

// MARK: - UITableViewDataSource, UITableViewDelegate
extension SongsListViewController: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int {
        return presenter?.songs.count ?? 0
    }

    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: SongCell.reuseId)
            as? SongCell,
              let oneSong = presenter?.songs[indexPath.row] else {
            return UITableViewCell()
        }
        cell.configure(with: oneSong)
        cell.songCellDelegate = self
        return cell
    }

    func tableView(_ tableView: UITableView,
                   heightForRowAt indexPath: IndexPath) -> CGFloat {
        return SongCell.height
    }
}
