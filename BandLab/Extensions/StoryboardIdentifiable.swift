//
//  StoryboardIdentifiable.swift
//  BandLab
//
//  Created by Maksim Kupetskii on 07/04/2018.
//  Copyright © 2018 Maksim Kupetskii. All rights reserved.
//

import UIKit

protocol StoryboardIdentifiable {
    static var storyboardIdentifier: String { get }
}

extension StoryboardIdentifiable where Self: UIViewController {
    static var storyboardIdentifier: String {
        return String(describing: self)
    }
}

extension UIStoryboard {
    func viewController<T: UIViewController>(_ type: T.Type) -> T {
        // swiftlint:disable identifier_name
        guard let vc = instantiateViewController(withIdentifier: type.storyboardIdentifier) as? T else {
            fatalError("No ViewController with Identifier \(type.storyboardIdentifier)")
        }
        return vc
    }
}

extension UIViewController: StoryboardIdentifiable {}
