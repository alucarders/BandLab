//
//  Constants.swift
//  BandLab
//
//  Created by Maksim Kupetskii on 07/04/2018.
//  Copyright © 2018 Maksim Kupetskii. All rights reserved.
//

import UIKit

public enum Constants {

    enum Storyboards {
        static let main = "Main"
    }

    enum Networking {
        static let contentType      = "Content-Type"
        static let jsonContentType  = "application/json"
        static let audioContentType = "audio/mpeg"
        static let urlEncodingType  = "application/x-www-form-urlencoded"
        static let charsetUTF8      = "charset=utf-8"

        static let googleDrive     = "drive.google.com"
        static let googleDriveOpen = "open"
        static let googlePathFile  = "file"
        static let googleDriveId   = "id="

        static let gDriveLink = "https://www.googleapis.com/drive/v3/files/"
        static let gApiKey    = "/?key=AIzaSyBCnxAY28i8CvnLy5_gRGIJtdoXSZ4uLbw&alt=media"
    }

    enum Colors {
        static let cellBackground = UIColor(red: 40/255.0,
                                            green: 44/255.0,
                                            blue: 51/255.0,
                                            alpha: 0.5)
        static let loaderColor = UIColor(red: 208/255.0,
                                         green: 2/255.0,
                                         blue: 27/255.0,
                                         alpha: 1)
        static let cellFont = UIColor.white
        static let refreshFont = UIColor.black
    }

    enum Fonts {
        static let cellFont = UIFont(name: "Helvetica", size: 26)
        static let refreshFont = UIFont(name: "Helvetica", size: 16)
    }
}
