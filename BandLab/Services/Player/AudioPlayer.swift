//
//  AudioPlayer.swift
//  BandLab
//
//  Created by Maksim Kupetskii on 08/04/2018.
//  Copyright © 2018 Maksim Kupetskii. All rights reserved.
//

import Foundation
import AVFoundation

protocol AudioPlayer {
    func playFile(at fileUrl: URL)
    func pause()
}

class SongPlayer {
    static var player: AVAudioPlayer?
}

extension SongPlayer: AudioPlayer {

    func playFile(at fileUrl: URL) {
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            try AVAudioSession.sharedInstance().setActive(true)

            if SongPlayer.player?.url != fileUrl {
                SongPlayer.player = try AVAudioPlayer(contentsOf: fileUrl)
            }
            SongPlayer.player?.play()
        } catch {
            print(error.localizedDescription)
        }
    }

    func pause() {
        SongPlayer.player?.pause()
    }
}
