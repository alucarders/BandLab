//
//  HTTPTask.swift
//  BandLab
//
//  Created by Maksim Kupetskii on 07/04/2018.
//  Copyright © 2018 Maksim Kupetskii. All rights reserved.
//

import Foundation

public typealias HTTPHeaders = [String: String]

public enum HTTPTask {
    case request
    case download
}
