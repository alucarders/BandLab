//
//  NetworkRouter.swift
//  BandLab
//
//  Created by Maksim Kupetskii on 07/04/2018.
//  Copyright © 2018 Maksim Kupetskii. All rights reserved.
//

import Foundation

public typealias NetworkRouterCompletion = (_ data: Data?,
    _ response: URLResponse?,
    _ error: Error?) -> Void
public typealias NetworkDownloadCompletion = (_ location: URL?,
    _ response: URLResponse?,
    _ error: Error?) -> Void

protocol NetworkRouter: class {
    associatedtype EndPoint: EndPointType
    func request(_ route: EndPoint, completion: @escaping NetworkRouterCompletion)
    func download(_ route: EndPoint, completion: @escaping NetworkDownloadCompletion)
}
